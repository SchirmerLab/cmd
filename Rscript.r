#Preparation
#Loading the required libraries
library(curatedMetagenomicData)
library(tidyverse)
library(vegan)
library(reshape2)
​
#Check your working directory
getwd()


#create a new directory for the workshop and change the current working directory to the new directory
dir.create("~/multi-omics_tutorial")
setwd("~/multi-omics_tutorial")

#Exploration of the data structure
#Exploring the data in the curatedMetagenomicsData package
#All columns
colnames(curatedMetagenomicData::combined_metadata)

#First 30
head(colnames(curatedMetagenomicData::combined_metadata), 30)


#Number of studies in the cMD package
curatedMetagenomicData::combined_metadata %>%
  pull(study_name) %>%
  unique() %>%
  length


#Number of samples in each study
curatedMetagenomicData::combined_metadata %>%
  pull(study_name) %>%
  table()


#Available microbiome profiles for all studies
curatedMetagenomicData("")

#Access profiles of one selected study
#Showing for only HMP_2012 study, for this workshop we will use the data from **HMP_2012** study
curatedMetagenomicData("HMP_2012")

#Data extraction
#Filter the metadata by study name
#Extract metadata for only HMP_2012 study

metadata_filtered <- combined_metadata %>%
                         filter(study_name == "HMP_2012")

#Looking for the count of the unique values in the columns
table(metadata_filtered$body_site)​
table(metadata_filtered$body_subsite)


#Filter for body_subsite
metadata_filtered <- metadata_filtered %>%
#filter for body subsite
  filter(body_subsite == "supragingival_plaque" | body_subsite == "stool") %>%
  select(where(~ !all(is.na(.x))))


#Removing longitudinal data, if any
pasted <- paste0(metadata_filtered[, "subject_id"], metadata_filtered[, "body_site"])
metadata_filtered$pasted <- pasted

#Getting only the 1st match to the 'pasted' value in the metadata filtered and returning the rownames
rows_to_extract <- unique(as.data.frame(sapply(pasted, function(x) match(x, metadata_filtered$pasted))))[,1]

#Filtering the metadata_filtered further for removing the longitudinal data.
#When more than one sample from the same subject for the same body_subsite exists, take only the first one
#Removing the column "pasted"
metadata_filtered <- metadata_filtered[rows_to_extract, !(names(metadata_filtered) %in% "pasted")]

head(metadata_filtered)
nrow(metadata_filtered)


#Extracting relative abundance for HMP_2012 from the cMD using the function curatedMetagenomicData()
tse_hmp_2012 <- curatedMetagenomicData("HMP_2012.relative_abundance", dryrun = FALSE)[[1]]


#Extract the main assay from the tse by name
relabs_hmp_2012 <- assay(tse_hmp_2012, "relative_abundance")

#check the rownames of the extracted assay
head(rownames(relabs_hmp_2012))

#Shorten rownames ####
rownames(relabs_hmp_2012) = gsub(".*s__", "", rownames(relabs_hmp_2012))

#Get only the samples which are in the metadata_filtered 
relabs_hmp_2012_sub <- relabs_hmp_2012[,colnames(relabs_hmp_2012) %in% metadata_filtered$sample_id]


#Check if the order of the samples in the data (relabs_hmp_2012_sub) and metadata_filtered is same
metadata_filtered_ordered <- metadata_filtered[sapply(colnames(relabs_hmp_2012_sub), function(x) grep(x, metadata_filtered$sample_id)),]
sum(!(metadata_filtered_ordered$sample_id == colnames(relabs_hmp_2012_sub)))


#Estimating diversity
#Calculating the alpha diversity using the diversity function (vegan package) and 'Shannon index'
alpha_div <- data.frame(diversity(t(relabs_hmp_2012_sub), index = "shannon"))
colnames(alpha_div) <- c("shannon_div")

#Adding the information on body subsite. Changing to TitleCase: stool -> Stool and supragingival_plaque: Supragingival Plaque
alpha_div$body_subsite <- tools::toTitleCase(gsub("_", " ", metadata_filtered_ordered[sapply(rownames(alpha_div), function(x) grep(x, metadata_filtered_ordered$sample_id)),]$body_subsite))
head(alpha_div)

#Make the plot
p <- ggplot(alpha_div, aes(x = body_subsite, y = shannon_div, fill = body_subsite)) + theme_bw() +
  geom_boxplot(outlier.shape = NA) +
  geom_point(size = 3, alpha = 0.5) + ggtitle("Alpha diversity (Shannon index)") + xlab("Body subsite") + ylab("Shannon diversity") +
  theme(axis.title.x = element_text(size = 14), axis.text = element_text(size = 14), axis.text.y = element_text(size = 14), 
  axis.title.y = element_text(size = 14), plot.title = element_text(size = 14, hjust = 0.5), legend.position = "none") + 
  scale_fill_manual(name = "Body site", values = c("#EF8A62", "#67A9CF"))

#Save the plot in pdf
ggsave("alpha.pdf", p, width= 4, height= 5, unit="in", device= "pdf")


#Calculating the beta diversity using vegdist function (vegan package) and method 'Bray'
#cmdscale returns a set of points such that the distances between the points are equal to the dissimilarities
beta_div <- vegdist(t(relabs_hmp_2012_sub), method = "bray") %>%
                    cmdscale(eig = TRUE)

#Creating a dataframe of the cmdscale returned points
pcoa = data.frame(PCoA1 = beta_div$points[,1], PCoA2 = beta_div$points[,2])

#Adding body subsite to the dataframe
pcoa$body_subsite = tools::toTitleCase(gsub("_", " ", metadata_filtered[sapply(rownames(pcoa), function(x) grep(x, metadata_filtered$sample_id)),]$body_subsite))
head(pcoa)

#Make the plot
p <- ggplot(pcoa, aes(x = PCoA1, y = PCoA2)) + 
    geom_point(aes(colour = body_subsite), size = 3) + theme_bw() + theme(legend.position = "bottom") + 
    ggtitle("PCoA with Bray-Curtis") + xlab("PC1") + ylab("PC2") +
    theme(axis.title.x = element_text(size = 14), axis.text = element_text(size = 14), axis.text.y = element_text(size = 14), 
    axis.title.y = element_text(size = 14), plot.title = element_text(size = 14, hjust = 0.5), legend.position = "bottom") + 
    geom_hline(yintercept = 0,linetype = "dashed", colour = "grey") + 
    geom_vline(xintercept = 0,linetype = "dashed", colour = "grey") +
    scale_colour_manual(name = "Body site", values = c("#EF8A62", "#67A9CF"))

#Save the plot in pdf
ggsave("beta.pdf", p, width= 5, height= 2.5, device= "pdf")

#Performing permanova to see the partiiton between the data (samples) for body_subsite

#Run permanova
#Performing permanova using the function adonis (vegan package)
permanova <- vegan::adonis(t(relabs_hmp_2012_sub) ~ body_subsite,
                data = metadata_filtered_ordered,
                permutations = 1999)

permanova


#Relative abundance transformation
#Define an arcsine function to transform the relative abundances to correct for compositionality-related issues

asin_trans <- function(rel_ab){
  return(asin(sqrt(rel_ab/100)))
}

#Apply the transformation
relabs_hmp_2012_sub_arc <- apply(relabs_hmp_2012_sub, c(1,2), function(x) asin_trans(x))


#Filtering for prevalent species

#Total number of species
nrow(relabs_hmp_2012_sub_arc)

#Set a prevalence filter
prevalence_threshold <- 20

#remove all below the prevalence filter
relabs_hmp_2012_sub_arc_filtered <- relabs_hmp_2012_sub_arc[rowSums(relabs_hmp_2012_sub_arc > 0) > prevalence_threshold, ]

# Number of species after the filtration
nrow(relabs_hmp_2012_sub_arc_filtered)



#Fitting a linear regression model using body_subsite, gender and age
species <- rownames(relabs_hmp_2012_sub_arc_filtered)

age <- metadata_filtered_ordered$age
gender <- metadata_filtered_ordered$gender
body_subsite <- metadata_filtered_ordered$body_subsite

#Defining the function for linear model
compute_lm <- function(species_relabs){

  my_df = data.frame(my_species = species_relabs, 
                     body_subsite = body_subsite,
                     gender = gender,
                     age = age)

#Using broom::tidy to generate clean output
  res_lm = broom::tidy(lm(as.formula("my_species ~ body_subsite + gender + age") , data=my_df, na.action = na.omit))
  res_lm = column_to_rownames(res_lm, var = "term")
  res = res_lm["body_subsitesupragingival_plaque", "p.value"]

  return(res)
}

#Running the linear model
lmResults <- as.data.frame(
  sapply(species, 
         FUN = function(x) {
           species_relabs <- relabs_hmp_2012_sub_arc_filtered[rownames(relabs_hmp_2012_sub_arc_filtered) ==x,] 
           res <- compute_lm(species_relabs)
           return(res)
         })
  )

colnames(lmResults) <- c("p.value")

#FDR-correction with Benjamini-Hochberg (BH)
lmResults$q_values <- p.adjust(lmResults$p.value,method = "BH")

#Look at the top species associated with stool or supragingival plaque; sort by q-value
head(lmResults[order(lmResults$q_values),], n = 6)[,"q_values", drop=FALSE]

#Taking top 3
my_lm_species <- rownames(head(lmResults[order(lmResults$q_values),], n = 3))
my_lm_species

#Get abundances of these species and bodysite for each sample
my_data = data.frame(t(relabs_hmp_2012_sub_arc_filtered[rownames(relabs_hmp_2012_sub_arc_filtered) %in% my_lm_species,]))
my_data$body_subsite = tools::toTitleCase(gsub("_", " ", sapply(rownames(my_data), function(x) metadata_filtered$body_subsite[grep(x, metadata_filtered$sample_id)])))
head(my_data)

#reshapping for the ggplot
my_data_plot = melt(my_data)
#head(my_data_plot)

#Make the plot
p <- ggplot(my_data_plot, aes(x=variable, y=value, fill=body_subsite)) + 
     geom_boxplot(aes(fill=body_subsite)) + theme_bw() + 
     theme(axis.title.x = element_blank(), axis.text.x = element_text(size = 14, angle = 45,  hjust=1), axis.text.y = element_text(size = 14), 
     axis.title.y = element_text(size = 14), plot.title = element_text(size = 14, hjust = 0.5), legend.position = "bottom") + 
    scale_fill_manual(name="Bodysite", values = c("#EF8A62", "#67A9CF")) +
     labs(title="Bodysite-specific species", y = "Relative abundance")

#Save the plot in pdf
ggsave("bodysite_specific.pdf", p, width= 8, height= 4, device= "pdf")




#Extras
#When extracting more than one study use

returnSamples()
#or
mergeData()

#Taxonomy ranks available for the data using the function taxonomyRanks (mia package)
library(mia)
taxonomyRanks(tse_hmp_2012)

#The command "curatedMetagenomicData("HMP_2012.relative_abundance", dryrun = FALSE)1" returns the data at the species level.
#If we want relative abundance at a different taxonomic level and save it into the S4 data structure

altExps(tse_hmp_2012) <- splitByRanks(tse_hmp_2012, "Phylum")
Accessing the alternate Experiments

#Names of assays in altExps
names(altExps(tse_hmp_2012))

#Returning an assay from altExps
assay(altExps(tse_hmp_2012)[[1]])


